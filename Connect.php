<?php
namespace bdb\facebook;

use yii;
use bdb\facebook\FacebookSession;
use bdb\facebook\FacebookRedirectLoginHelper;
use bdb\facebook\FacebookRequest;
use bdb\facebook\FacebookRequestException;
use bdb\facebook\GraphUser;
use bdb\facebook\GraphObject;

use common\models\AuthAccount;
use yii\web\UploadedFile;


Class Connect  
{
 
	public $helper;   
	public $me;
	public $permission; 
	public $method = 'GET';
	public $request = '/me';

	public $loginForm;
	public $account;


	//public $method = 'DELETE';
	//public $request = '/897337363659586/permissions';
	

	public function  __construct($loginForm, $autoLogin = true)
	{
		if (!isset($_SESSION)) session_start();

		$this->permission = Yii::$app->params['facebook']['permission'];
		$this->loginForm = $loginForm;
		$this->account = new AuthAccount;

		FacebookSession::setDefaultApplication(Yii::$app->params['facebook']['appId'], Yii::$app->params['facebook']['appSecret']);

		$this->helper = new FacebookRedirectLoginHelper(Yii::$app->params['facebook']['redirect']);
		if($autoLogin) $this->login();
	}

	public function login()
	{ 
		if($this->isLoggedIn()) return true;

		try {
          $session = $this->helper->getSessionFromRedirect();
        } catch(FacebookRequestException $ex) {
            //
        } catch(\Exception $ex) {
            //
        }

        if (isset($session)) {


        	$FR = $this->FacebookRequest($session);
            if($FR) $_SESSION['fbToken'] = $session->getToken();

         	return $FR;
        } else return false;

	}

	public function renderLoginButton()
	{
         echo '<a href="' .$this->getLoginUrl(). '">Login with Facebook</a>';
	}

	
	public function isLoggedIn()
	{    
	    if(isset($_SESSION['fbToken']))
	    {
			return $this->FacebookRequest(self::getSession());        
	    } 
	    else return false;
	}


	public function getLoginUrl()
	{
		return $this->helper->getLoginUrl(array( 'scope' => $this->permission));
	}


	static function getSession ()
	{
		return new FacebookSession($_SESSION['fbToken']);
	}


	static function getUrlAvatar($uid)
	{	
		if(isset($_SESSION['fbToken']))
	    {
		    // Get User’s Profile Picture
			$request = ( new FacebookRequest(self::getSession(), 'GET', '/'.$uid.'/picture?type=large&redirect=false' ) )->execute();
			// Get response as an array
			$picture = $request->getGraphObject(GraphObject::className())->asArray();
			return $picture['url'];
		} else return false;
	}



	public function saveAvatar($url)
	{
		if($url) {
			$name = \Yii::$app->security->generateRandomString().'.jpg';
			$img = \Yii::$app->params['uploadPath']. '\\'.$this->loginForm->model->tableName().'\\avatar\\'.$name;
			//if(file_put_contents($img, file_get_contents($url))) return $name; else return '';
			return $name;
		}
	}

	public function verifyAuthAccount($accessToken)
	{

		$account = $this->account->find()->where('email = :email AND deleted = :deleted AND active = :active', ['email'=>$this->me->getProperty('email'), 'deleted' => 0, 'active' => 1])->one();
 		if(is_null($account))
			return $this->createAuthAccount($accessToken);
		else 
 		{
			$this->account = $account;
			return $this->registerWebSession($accessToken);
 		}
	}

	public function createAuthAccount($accessToken)
	{
 
		//AuthAccount
		$this->account->email = $this->me->getProperty('email');
		$this->account->password = "#";
		$this->account->externalAuthSource = "FACEBOOK";
		$this->account->externalAuthSourceId = $this->me->getProperty('id');
		$this->account->externalAuthSourceAccessToken = $accessToken;

		if($this->account->save())
		{
			//User
			$this->loginForm->model->authAccountId = $this->account->id;
			$this->loginForm->model->name = $this->me->getProperty('name');
			$this->loginForm->model->avatar = self::saveAvatar(self::getUrlAvatar($this->me->getProperty('id')));
			$this->loginForm->model->gender = strtoupper($this->me->getProperty('gender'));
			if($this->loginForm->model->save())
			{
				$this->registerWebSession($accessToken);
				return true;
			} 
			else {$this->account->delete(); return false; }
		} else return false;
	}

//---------------------------------------------------------------------

	protected function FacebookRequest($session)
	{
 		try {
			$this->me = (new FacebookRequest(
	        	$session, $this->method, $this->request
	        ))->execute()->getGraphObject(GraphUser::className());

	        return $this->verifyAuthAccount($session->getToken());

		}catch(\Exception $ex) {
            
            session_unset();
        	session_destroy();
        	print '<pre>';
        	var_dump($ex);
            return false;

        }

 	} 	


	protected function registerWebSession($accessToken)
	{	
		$this->loginForm->email = $this->account->email;
		$user = $this->loginForm->getUser();
		if(!is_null($user))
			return Yii::$app->user->login($user,  3600 * 24 * 30 );
		else return false;
		
	}



}
 